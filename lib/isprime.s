; Check if given number is prime
isprime:
    MOV bp, sp
    MOV bx, [bp + 2]
    CMP bx, 2
    JB isprimeN
    JE isprimeY

    PUSH bx
    CALL isqrt
    POP bx
    MOV bp, ax

    MOV cx, 2
isprimeloop:
        XOR dx, dx
        MOV ax, bx
        DIV cx
        TEST dx, dx
        JZ isprimeN
        INC cx
        CMP cx, bp
        JBE isprimeloop

isprimeY:
    MOV ax, 1
    RET

isprimeN:
    XOR ax, ax
    RET

%include "isqrt.s"
