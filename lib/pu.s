; Print a number
pu:
    MOV bp, sp
    XOR dx, dx
    MOV ax, [bp + 2]
    MOV bx, 10
    DIV bx
    TEST ax, ax
    JZ puchr

    PUSH dx
    PUSH ax
    CALL pu
    POP ax
    POP dx

puchr:
    MOV ah, 0x02
    ADD dl, '0'
    INT 0x21
    RET
