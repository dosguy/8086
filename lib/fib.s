; Return the Nth Fibonacci number
fib:
    MOV bp, sp
    MOV cx, [bp + 2]

    CMP cx, 2
    JB fibN

    XOR ax, ax

    CMP cx, 24
    JA fiboverflow

    MOV bx, 1
    MOV dx, bx
    DEC cx
fibloop:
        ADD dx, ax
        MOV ax, bx
        MOV bx, dx
        LOOP fibloop
    MOV ax, dx
    RET

fibN:
    MOV ax, cx
    RET

fiboverflow:
    NOT ax
    RET
