%macro utlpc 1
    MOV ah, 0x02
    MOV dl, %1
    INT 0x21
%endmacro

%macro utlps 1
    MOV ah, 0x09
    MOV dx, %1
    INT 0x21
%endmacro

%macro utlcall1 3
    PUSH %1
    CALL %2
    POP %3
%endmacro
