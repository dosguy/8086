; Print a number (in lowercase string form)

%include "utl.s"

pus:
    MOV bp, sp
    MOV ax, [bp + 2]
    CMP ax, 20
    JB pusD
    CMP ax, 100
    JB pusC
    CMP ax, 1000
    JB pusB

pusA:
    XOR dx, dx
    MOV bx, 1000
    DIV bx
    PUSH dx
    utlcall1 ax, pus, ax
    POP cx
    utlps pusS2
        TEST cx, cx
        JZ pusAend
        utlpc ' '
        utlcall1 cx, pus, cx
pusAend:
    RET

pusB:
    MOV bx, 100
    DIV bl
    PUSH ax
    XOR ah, ah
    utlcall1 ax, pus, ax
    POP bx
    utlps pusS1
        TEST bh, bh
        JZ pusBend
        utlpc ' '
        SHR bx, 8
        utlcall1 bx, pus, bx
pusBend:
    RET

pusC:
    MOV bx, 10
    DIV bl
    MOV cl, ah
    MOV bl, al
    SUB bx, 2
    SHL bx, 1
    utlps [pusT + bx]
        TEST cl, cl
        JZ pusCend
        utlpc '-'
        XOR ch, ch
        utlcall1 cx, pus, cx
pusCend:
    RET

pusD:
    MOV bx, ax
    SHL bx, 1
    utlps [pusN + bx]
    RET

pusN0 DB "zero$"
pusN1 DB "one$"
pusN2 DB "two$"
pusN3 DB "three$"
pusN4 DB "four$"
pusN5 DB "five$"
pusN6 DB "six$"
pusN7 DB "seven$"
pusN8 DB "eight$"
pusN9 DB "nine$"
pusN10 DB "ten$"
pusN11 DB "eleven$"
pusN12 DB "twelve$"
pusN13 DB "thirteen$"
pusN14 DB "fourteen$"
pusN15 DB "fifteen$"
pusN16 DB "sixteen$"
pusN17 DB "seventeen$"
pusN18 DB "eighteen$"
pusN19 DB "nineteen$"
pusT1 DB "twenty$"
pusT2 DB "thirty$"
pusT3 DB "fourty$"
pusT4 DB "fifty$"
pusT5 DB "sixty$"
pusT6 DB "seventy$"
pusT7 DB "eighty$"
pusT8 DB "ninety$"
pusS1 DB " hundred$"
pusS2 DB " thousand$"
pusN DW pusN0, pusN1, pusN2, pusN3, pusN4, pusN5, pusN6, pusN7, pusN8, pusN9,\
pusN10, pusN11, pusN12, pusN13, pusN14, pusN15, pusN16, pusN17, pusN18, pusN19
pusT DW pusT1, pusT2, pusT3, pusT4, pusT5, pusT6, pusT7, pusT8
