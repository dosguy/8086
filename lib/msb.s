; Most Significant Bit
; 0001 0101 1101 0110 -> 0001 0000 0000 0000
; Using bit smearing
msb:
    MOV bp, sp
    MOV ax, [bp + 2]
    MOV bx, ax
    SHR bx, 1
    OR ax, bx
    MOV bx, ax
    SHR bx, 2
    OR ax, bx
    MOV bx, ax
    SHR bx, 4
    OR ax, bx
    MOV bx, ax
    SHR bx, 8
    OR ax, bx
    MOV bx, ax
    SHR bx, 1
    XOR ax, bx
    RET
