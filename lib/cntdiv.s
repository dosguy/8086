; Count how many proper divisors a number has
cntdiv:
    MOV bp, sp
    MOV bx, [bp + 2]
    PUSH bx
    CALL isqrt
    POP bx
    MOV cx, ax
    XOR bp, bp
    JCXZ cntdivend
    XOR dx, dx
    MOV ax, bx
    DIV cx
    TEST dx, dx
    JNZ cntdivloop
    CMP ax, cx
    JNE cntdivloop
    INC bp
    DEC cx
    JCXZ cntdivend
cntdivloop:
        XOR dx, dx
        MOV ax, bx
        DIV cx
        TEST dx, dx
        JNZ cntdivloopcontinue
        ADD bp, 2
cntdivloopcontinue:
        LOOP cntdivloop
cntdivend:
    MOV ax, bp
    RET

%include "isqrt.s"
