; xorshift
rng:
    MOV ax, [rngS]
    MOV bx, ax

    SHL bx, 7
    XOR ax, bx
    MOV bx, ax

    SHR bx, 9
    XOR ax, bx
    MOV bx, ax

    SHL bx, 8
    XOR ax, bx

    RET

rngSeed:
    MOV bp, sp
    MOV [rngS], [bp + 2]
    RET

rngS DW 1
