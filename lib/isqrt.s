; Return the integer square root of a number
; bp - n
; di - guess
; ax - guess square
; dx - reserved for multiplication
; bx - low bound
; cx - high bound

isqrt:
    MOV bp, sp
    MOV bp, [bp + 2]
    MOV di, bp
    SHR di, 1
    MOV bx, 0
    MOV cx, bp

isqrtloop:
        CMP bx, cx
        JE isqrtdone

        MOV ax, di
        XOR dx, dx
        MUL ax
        JO isqrthi
        CMP ax, bp
        JA isqrthi
        JE isqrtdone

isqrtlo:
            MOV bx, di
            JMP isqrtguess

isqrthi:
            MOV cx, di
            DEC cx

isqrtguess:
        MOV ax, bx
        ADD ax, cx
        MOV dx, ax
        AND ax, 1
        JZ isqrtguesseven

isqrtguessodd:
            INC dx

isqrtguesseven:
            SHR dx, 1
            MOV di, dx

        JMP isqrtloop

isqrtdone:
    MOV ax, di
    RET
