; Print a number (formatted)
; Arguments: number, minimum width, padding character
puf:
    MOV bp, sp
    MOV bx, [bp + 6]
    PUSH bp
    PUSH bx
    CALL cntdig
    POP bx
    POP bp
    MOV cx, [bp + 4]
    MOV dx, [bp + 2]

    CMP ax, cx
    JAE pufpu
    SUB cx, ax
    MOV ah, 0x02
pufpad:
        INT 0x21
        LOOP pufpad

pufpu:
    PUSH bx
    CALL pu
    POP bx
    RET

%include "../lib/cntdig.s"
%include "../lib/pu.s"
