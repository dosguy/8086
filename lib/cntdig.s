; Count how many decimal digits a number has
cntdig:
    MOV bp, sp
    MOV ax, [bp + 2]
    MOV bx, 10
    XOR cx, cx
cntdigloop:
        XOR dx, dx
        INC cx
        DIV bx
        TEST ax, ax
        JZ cntdigloopend
        JMP cntdigloop
cntdigloopend:
    MOV ax, cx
    RET
