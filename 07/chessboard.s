%define W 8
%define H 8
%define WHITE '#'
%define BLACK '-'
%define WXORB WHITE^BLACK

ORG 0x100

MOV ah, 0x02
MOV cx, H
MOV dh, WHITE
row:
    MOV bx, cx

    MOV cx, W
    MOV dl, dh
col:
        INT 0x21
        XOR dl, WXORB
        LOOP col

    MOV cx, bx
    XOR dh, WXORB
    MOV dl, 10
    INT 0x21
    LOOP row

RET
