%define N 10

ORG 0x100

MOV cx, N
countdown:
    PUSH cx
    CALL pu
    POP cx
    MOV ah, 0x02
    MOV dl, 10
    INT 0x21
    LOOP countdown

MOV ah, 0x09
MOV dx, string
INT 0x21

RET

string DB "Liftoff!$"

%include "../lib/pu.s"
