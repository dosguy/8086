ORG 0x100

MOV cx, 1
countingloop:
    PUSH cx
    CALL pu
    MOV ah, 0x09
    MOV dx, string
    INT 0x21
    CALL pus
    POP cx

    MOV ah, 0x02
    MOV dl, 10
    INT 0x21

    INC cx
    JCXZ countingloopend
    JMP countingloop
countingloopend:

RET

string DB " is $"

%include "../lib/pu.s"
%include "../lib/pus.s"
