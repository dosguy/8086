ORG 0x100

MOV cx, 100
MOV bx, 1
XOR si, si
outerloop:
innerloop:
        NOT BYTE [doors + si]
        ADD si, bx
        CMP si, 100
        JB innerloop
    INC bx
    MOV si, bx
    DEC si
    LOOP outerloop

MOV ah, 0x02
MOV cx, 100
XOR si, si
printloop:
    MOV dl, [doors + si]
    ADD dl, '1'
    INT 0x21
    INC si
    LOOP printloop

RET

doors TIMES 100 DB 0xFF
