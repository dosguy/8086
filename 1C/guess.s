ORG 0x100

XOR ah, ah
INT 0x1A
MOV ax, dx
XOR dx, dx
MOV bx, 10
DIV bx
MOV bx, dx

guessloop:
    MOV ah, 0x09
    MOV dx, msg0
    INT 0x21

    MOV ah, 0x08
    INT 0x21

    MOV ah, 0x02
    MOV dl, al
    INT 0x21

    SUB al, '0'
    CMP al, bl
    JE guessloopend

    MOV ah, 0x09
    MOV dx, msg1
    INT 0x21

    JMP guessloop
guessloopend:
MOV ah, 0x09
MOV dx, msg2
INT 0x21
RET

msg0 DB "Guess a number from 0 to 9: $"
msg1 DB 10, "Incorrect.", 10, "$"
msg2 DB 10, "Correct!$"

%include "../lib/pu.s"
