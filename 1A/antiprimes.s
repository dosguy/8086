ORG 0x100

MOV cx, 1
XOR bx, bx
XOR dx, dx
searchloop:
    PUSH bx
    PUSH dx
    PUSH cx
    CALL cntdiv
    POP cx
    POP dx
    POP bx
    CMP ax, dx
    JBE searchloopcontinue
        MOV dx, ax
        MOV bx, cx
        PUSH cx
        PUSH dx
        PUSH bx
        CALL pu
        MOV ah, 0x02
        MOV dl, 10
        INT 0x21
        POP bx
        POP dx
        POP cx
searchloopcontinue:
    INC cx
    JCXZ searchloopend
    JMP searchloop
searchloopend:
RET

%include "../lib/cntdiv.s"
%include "../lib/pu.s"
