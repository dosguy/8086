ORG 0x100

XOR cx, cx
fizzbuzzloop:
    INC cx

    MOV ax, cx
    XOR dx, dx
    MOV bx, 15
    DIV bx
    TEST dx, dx
    JZ fizzbuzz

    MOV ax, cx
    XOR dx, dx
    MOV bx, 5
    DIV bx
    TEST dx, dx
    JZ buzz

    MOV ax, cx
    XOR dx, dx
    MOV bx, 3
    DIV bx
    TEST dx, dx
    JZ fizz

number:
        PUSH bx
        PUSH cx
        CALL pu
        POP cx
        POP bx
        JMP fizzbuzzloopcontinue

fizzbuzz:
        MOV ah, 0x09
        MOV dx, sfizz
        INT 0x21
        MOV dx, sbuzz
        INT 0x21
        JMP fizzbuzzloopcontinue

fizz:
        MOV ah, 0x09
        MOV dx, sfizz
        INT 0x21
        JMP fizzbuzzloopcontinue

buzz:
        MOV ah, 0x09
        MOV dx, sbuzz
        INT 0x21
        JMP fizzbuzzloopcontinue

fizzbuzzloopcontinue:
    MOV ah, 0x02
    MOV dl, 10
    INT 0x21
    CMP cx, 100
    JB fizzbuzzloop

RET

sfizz DB "Fizz$"
sbuzz DB "Buzz$"

%include "../lib/pu.s"
