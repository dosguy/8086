%define W 80

ORG 0x100

MOV bx, 0
MOV cx, 2
PUSH bx
primeloop:

    PUSH cx
    CALL isprime
    POP cx
    TEST ax, ax
    JZ primeloopcontinue

        PUSH cx
        CALL cntdig
        POP cx
        POP bx
        ADD bx, ax
        CMP bx, W
        JB primeloopinline

primeloopnextline:
            PUSH ax
            MOV dl, 10
            JMP primeloopprint

primeloopinline:
            INC bx
            PUSH bx
            MOV dl, ' '

primeloopprint:
        MOV ah, 0x02
        INT 0x21
        PUSH cx
        CALL pu
        POP cx

primeloopcontinue:
    INC cx
    JNZ primeloop

POP bx
RET

%include "../lib/isprime.s"
%include "../lib/cntdig.s"
%include "../lib/pu.s"
