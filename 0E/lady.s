ORG 0x100

MOV ah, 0x09
MOV cx, 8
XOR bx, bx
outerloop:
    MOV dx, s1
    INT 0x21
    MOV dx, [a + bx]
    INT 0x21
    MOV dx, s2
    INT 0x21
    MOV dx, [n + bx]
    INT 0x21

    DEC cx
    JCXZ outerloop_end

    PUSH bx
    TEST bx, bx ; set ZF
innerloop:
        JZ innerloop_end

        CMP bx, 2
        JE innerloop_spider
        JMP innerloop_endif
innerloop_spider:
            MOV dx, s3
            INT 0x21
innerloop_endif:

        MOV dx, s4
        INT 0x21
        MOV dx, [a + bx]
        INT 0x21
        MOV dx, s5
        INT 0x21
        SUB bx, 2
        MOV dx, [a + bx]
        INT 0x21
        MOV dx, s2
        INT 0x21
        JMP innerloop
innerloop_end:
    POP bx

    MOV dx, s6
    INT 0x21

    ADD bx, 2
    JMP outerloop
outerloop_end:

RET

s1 DB "There was an old lady who swallowed a $"
s2 DB ";", 10, "$"
s3 DB "That wriggled and jiggled and tickled inside her!", 10, "$"
s4 DB "She swallowed the $"
s5 DB " to catch the $"
s6 DB "I don't know why she swallowed a fly - Perhaps she'll die!", 10, 10, "$"
a1 DB "fly$"
a2 DB "spider$"
a3 DB "bird$"
a4 DB "cat$"
a5 DB "dog$"
a6 DB "goat$"
a7 DB "cow$"
a8 DB "horse$"
n0 DB "$"
n3 DB "How absurd to swallow a bird!", 10, "$"
n4 DB "Imagine that! She swallowed a cat!", 10, "$"
n5 DB "What a hog, to swallow a dog!", 10, "$"
n6 DB "She just opened her throat and swallowed a goat!", 10, "$"
n7 DB "I don't know how she swallowed a cow!", 10, "$"
n8 DB "...She died, of course!", 10, "$"
a DW a1, a2, a3, a4, a5, a6, a7, a8
n DW n0, n0, n3, n4, n5, n6, n7, n8
