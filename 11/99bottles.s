ORG 0x100

MOV cx, 99
PUSH cx

mainloop:
    CALL pu
    MOV ah, 0x09
    MOV dx, str1
    INT 0x21
    CALL pu
    MOV ah, 0x09
    MOV dx, str2
    INT 0x21
    POP cx
    DEC cx
    PUSH cx
    CALL pu
    MOV ah, 0x09
    MOV dx, str1
    INT 0x21
    MOV ah, 0x02
    MOV dl, 10
    INT 0x21
    CMP cx, 2
    JNE mainloop
POP cx

MOV ah, 0x09
MOV dx, str3
INT 0x21

RET

%include "../lib/pu.s"

str1 DB " bottles of beer on the wall", 10, "$"
str2 DB " bottles of beer", 10, "Take one down, pass it around", 10, "$"
str3 DB "2 bottles of beer on the wall", 10, \
        "2 bottles of beer", 10, \
        "Take one down, pass it around", 10, \
        "1 bottle of beer on the wall", 10, 10, \
        "1 bottle of beer on the wall", 10, \
        "1 bottle of beer", 10, \
        "Take one down, pass it around", 10, \
        "No more bottles of beer on the wall", 10, 10, \
        "No more bottles of beer on the wall", 10, \
        "No more bottles of beer", 10, \
        "Go to the store and buy some more", 10, \
        "99 bottles of beer on the wall$"
