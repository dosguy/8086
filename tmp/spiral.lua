#!/usr/bin/env lua53

local W = 2
local H = 2
local N = W * H

local function spiral(w, h, x, y)
    return y < h and w + spiral(h - 1, w, h - y, x) or x
end

if N > 65535 then
    print("N is too big")
else
    local digits = N == 0 and 0 or math.floor(math.log(N, 10) + 0.5)
    local format = "%0" .. string.format("%d", digits + 1) .. "d "
    for y = H, 1, -1 do
        for x = 1, W do
            io.write(string.format(format, spiral(W, H, x, y)))
        end
        io.write("\n")
    end
end
