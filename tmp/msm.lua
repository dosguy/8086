#!/usr/bin/env lua53

-- Test the Middle Square Method of generating pseudo-random numbers.
-- The argument n must be a 16-bit unsigned integer.
-- Returns the length of the non-repeating pseudo-random number sequence.
local function msm(n)
    local original = n
    local set = {}
    local l = 0
    repeat
        set[n] = true
        n = ((n * n) & 0x00FFFF00) >> 8
        l = l + 1
    until set[n]
    return l
end

for i = 0, 65535 do
    print(i, msm(i))
end
