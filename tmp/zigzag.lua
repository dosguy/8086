#!/usr/bin/env lua53

local W = 5
local H = 6
local N = W * H

local function zigzag(w, h, x, y, d)
    local n = 1
    local X = 1
    local Y = h
    while X ~= x or Y ~= y do
        if d then
            if X < w then
                X = X + 1
                if Y < h then
                    Y = Y + 1
                else
                    d = not d
                end
            else
                Y = Y - 1
                d = not d
            end
        else
            if Y > 1 then
                Y = Y - 1
                if X > 1 then
                    X = X - 1
                else
                    d = not d
                end
            else
                X = X + 1
                d = not d
            end
        end
        n = n + 1
    end
    return n
end

if N > 65535 then
    print("N is too big")
else
    local digits = N == 0 and 0 or math.floor(math.log(N, 10) + 0.5)
    local format = "%0" .. string.format("%d", digits + 1) .. "d "
    for y = H, 1, -1 do
        for x = 1, W do
            io.write(string.format(format, zigzag(W, H, x, y, true)))
        end
        io.write("\n")
    end
end
