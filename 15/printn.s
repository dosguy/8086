%define N 324
%define W 5
%define P '0'

ORG 0x100

MOV ax, N
PUSH ax
MOV ax, W
PUSH ax
MOV ax, P
PUSH ax
CALL puf
ADD sp, 6

RET

%include "../lib/puf.s"
