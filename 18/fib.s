ORG 0x100

XOR bx, bx
MOV cx, 25
mainloop:
    PUSH cx
    PUSH bx
    CALL fib
    PUSH ax
    CALL pu
    MOV ah, 0x02
    MOV dl, 10
    INT 0x21
    POP ax
    POP bx
    POP cx
    INC bx
    LOOP mainloop

RET

%include "../lib/pu.s"
%include "../lib/fib.s"
