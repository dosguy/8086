%define W 9
%define H 5

ORG 0x100

MOV ah, 0x02
MOV cx, H
outerloop:
    MOV bx, cx

    MOV cx, W
    MOV dl, '#'
innerloop:
        INT 0x21
        LOOP innerloop

    MOV cx, bx
    MOV dl, 10
    INT 0x21
    LOOP outerloop

RET
