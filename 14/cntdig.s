%define N 1234

ORG 0x100

MOV bx, N
PUSH bx
CALL pu
CALL cntdig
POP bx
MOV bx, ax

MOV ah, 0x09
MOV dx, str0
INT 0x21

PUSH bx
CALL pu
POP bx

MOV ah, 0x09
MOV dx, str1
INT 0x21

CMP bx, 2
JB singular

MOV ah, 0x02
MOV dl, 's'
INT 0x21

singular:
RET

%include "../lib/cntdig.s"
%include "../lib/pu.s"

str0 DB " has $"
str1 DB " digit$"
