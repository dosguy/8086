%define N 5

ORG 0x100

MOV ah, 0x02
MOV cx, N
outerloop1:
    MOV bx, cx

    MOV dl, ' '
innerloop1:
    INT 0x21
    LOOP innerloop1

    MOV cx, N
    SUB cx, bx
    SHL cx, 1
    INC cx
    MOV dl, '*'
innerloop2:
    INT 0x21
    LOOP innerloop2

    MOV cx, bx
    MOV dl, 10
    INT 0x21
    LOOP outerloop1

MOV cx, N - 1
outerloop2:
    MOV bx, cx

    MOV cx, N + 1
    SUB cx, bx
    MOV dl, ' '
innerloop3:
        INT 0x21
        LOOP innerloop3

    MOV cx, bx
    SHL cx, 1
    DEC cx
    MOV dl, '*'
innerloop4:
        INT 0x21
        LOOP innerloop4

    MOV cx, bx
    MOV dl, 10
    INT 0x21
    LOOP outerloop2

RET
