ORG 0x100

MOV ah, 0x09
MOV dx, s0
INT 0x21

TEST ah, ah ; Make sure ZF=0
MOV cx, 11
MOV bx, 2
outerloop:
    MOV dx, s1
    INT 0x21
    MOV dx, [d + bx]
    INT 0x21
    MOV dx, s2
    INT 0x21
    PUSH bx
innerloop:
        MOV dx, [g + bx]
        INT 0x21
        JZ innerloop_end
        SUB bx, 2
        JMP innerloop
innerloop_end:
    POP bx
    ADD bx, 2
    LOOP outerloop

RET

s0 DB "On the first day of Christmas,", 10, \
      "My true love gave to me:", 10, \
      "A partridge in a pear tree.", 10, 10, "$"
s1 DB "On the $"
s2 DB " day of Christmas,", 10, "My true love gave to me:", 10, "$"
d2  DB "second$"
d3  DB "third$"
d4  DB "fourth$"
d5  DB "fifth$"
d6  DB "sixth$"
d7  DB "seventh$"
d8  DB "eighth$"
d9  DB "ninth$"
d10 DB "tenth$"
d11 DB "eleventh$"
d12 DB "twelfth$"
g1  DB "And a partridge in a pear tree.", 10, 10, "$"
g2  DB "Two turtle doves,", 10, "$"
g3  DB "Three french hens,", 10, "$"
g4  DB "Four calling birds,", 10, "$"
g5  DB "Five golden rings,", 10, "$"
g6  DB "Six geese a-laying,", 10, "$"
g7  DB "Seven swans a-swimming,", 10, "$"
g8  DB "Eight maids a-milking,", 10, "$"
g9  DB "Nine ladies dancing,", 10, "$"
g10 DB "Ten lords a-leaping,", 10, "$"
g11 DB "Eleven pipers piping,", 10, "$"
g12 DB "Twelve drummers drumming,", 10, "$"
d DW  0, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12
g DW g1, g2, g3, g4, g5, g6, g7, g8, g9, g10, g11, g12
