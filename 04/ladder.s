%define N 5

ORG 0x100

MOV ah, 0x02
MOV cx, N
printloop:
    MOV dl, '|'
    INT 0x21
    MOV dl, '-'
    INT 0x21
    MOV dl, '|'
    INT 0x21
    MOV dl, 10
    INT 0x21
    LOOP printloop

RET
