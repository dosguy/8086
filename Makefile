.POSIX:
.SUFFIXES:.s .com

DST=00/die1.com                                                     \
    00/die2.com                                                     \
    00/die3.com                                                     \
    00/die4.com                                                     \
    00/die5.com                                                     \
    01/42.com                                                       \
    02/emoji.com                                                    \
    03/powlvl.com                                                   \
    04/ladder.com                                                   \
    05/wing.com                                                     \
    06/rect.com                                                     \
    07/chessboard.com                                               \
    08/ramp.com                                                     \
    09/pyramid.com                                                  \
    0A/diamond.com                                                  \
    0B/100doors.com                                                 \
    0C/hello.com                                                    \
    0D/twelvedays.com                                               \
    0E/lady.com                                                     \
    0F/printn.com                                                   \
    10/launch.com                                                   \
    11/99bottles.com                                                \
    12/printn.com                                                   \
    13/count.com                                                    \
    14/cntdig.com                                                   \
    15/printn.com                                                   \
    16/undef.com                                                    \
    17/josephus.com                                                 \
    18/fib.com                                                      \
    19/primes.com                                                   \
    1A/antiprimes.com                                               \
    1B/fizzbuzz.com                                                 \
    1C/guess.com
LIB=lib/pu.s                                                        \
    lib/pus.s                                                       \
    lib/cntdig.s                                                    \
    lib/utl.s                                                       \
    lib/msb.s                                                       \
    lib/fib.s                                                       \
    lib/isqrt.s                                                     \
    lib/isprime.s                                                   \
    lib/cntdiv.s                                                    \
    lib/rng.s

all: $(DST)

$(DST): $(LIB)

.s.com:
	yasm -f bin -o $@ $<

distclean:
	rm -f $(DST)
