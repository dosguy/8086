%define N 12345

ORG 0x100

MOV cx, N
JCXZ error
JMP ok

error:
    MOV ah, 0x09
    MOV dx, errorstring
    INT 0x21
    MOV ax, 0x4C01
    INT 0x21

ok:
    PUSH cx
    CALL msb
    POP cx
    SUB cx, ax
    SHL cx, 1
    INC cx
    PUSH cx
    CALL pu
    POP cx
    RET

%include "../lib/msb.s"
%include "../lib/pu.s"

errorstring DB "N must not be zero.$"
