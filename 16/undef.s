ORG 0x100

PUSH ax
PUSH bx
PUSH cx
PUSH dx
PUSH si
PUSH di
PUSH bp
PUSH sp
PUSH ds
PUSH es
PUSH ss
PUSH cx
PUSHF

MOV cx, 13

printloop:

    MOV ah, 0x09
    MOV bx, cx
    SHL bx, 1
    MOV dx, [s + bx]
    INT 0x21

    POP ax
    PUSH cx
    PUSH ax
    CALL pu
    POP ax
    POP cx

    MOV ah, 0x02
    MOV dl, 10
    INT 0x21

    JCXZ printloopend
    DEC cx
    JMP printloop

printloopend:

JMP 0

%include "../lib/pu.s"

s0 DB "[sp]=$"
s1 DB "ax=$"
s2 DB "bx=$"
s3 DB "cx=$"
s4 DB "dx=$"
s5 DB "si=$"
s6 DB "di=$"
s7 DB "bp=$"
s8 DB "sp=$"
s9 DB "ds=$"
s10 DB "es=$"
s11 DB "ss=$"
s12 DB "cs=$"
s13 DB "flags=$"
s DW s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13
